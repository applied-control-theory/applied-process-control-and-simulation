# Applied Process Control and Simulation

In this project I would like to share my experience in the fields of process control and simulation in practice. Hopefully you will find it inspiring and useful.

The projects are all in IPython Notebook format. You can view them from within GitLab, but up to now (2022.07) formulas are sometimes broken. Hopefully that will improve in future. Alternatively you can view the output on GitLab Pages, or even better (as long as it is available) on nbviewer.org. Links are supplied for convenience.

GitLab does not start a `jupyter notebook` for you. To experiment, you need to download or clone the repository and start the notebooks locally. To do that

```bash
git clone ...
cd applied-process-control-and-simulation
jupyter-notebook
(open the link in your browser, usually http://localhost:8888)
```

## Projects

   * **Simple Harmonic Oscillator** as a sine wave source: This project explores mathematics required to synthesize a variable amplitude and frequency sine wave source.

     View on:
      - [GitLab Pages](https://lerichi.gitlab.io/applied-process-control-and-simulation/simple-harmonic-oscillator.html)
      - [nbviewer.org](https://nbviewer.org/urls/gitlab.com/lerichi/applied-process-control-and-simulation/-/raw/main/Simple%20harmonic%20oscillator.ipynb)
      - [Raw notebook](https://gitlab.com/lerichi/applied-process-control-and-simulation/-/blob/main/Simple%20harmonic%20oscillator.ipynb)

   * **Cart-pendulum motion using Lagrangian mechanics**: Derivation of the general equations of motion and simulation of a pendulum on a moving cart driven by an external force.

     View on:
      - [GitLab Pages](https://lerichi.gitlab.io/applied-process-control-and-simulation/pendulum-cart-simulation.html)
      - [nbviewer.org](https://nbviewer.org/urls/gitlab.com/lerichi/applied-process-control-and-simulation/-/raw/main/Pendulum-cart%20simulation.ipynb)
      - [Raw notebook](https://gitlab.com/lerichi/applied-process-control-and-simulation/-/blob/main/Pendulum-cart%20simulation.ipynb)

   * **Extended Kalman Filter for Synchro Transmitter**: An Extended Kalman Filter is used to track the angle and rotational speed of a synchro transmitter.

     View on:
      - [GitLab Pages](https://lerichi.gitlab.io/applied-process-control-and-simulation/synchro-kalman-observer.html)
      - [nbviewer.org](https://nbviewer.org/urls/gitlab.com/lerichi/applied-process-control-and-simulation/-/raw/main/synchro-kalman-observer.ipynb)
      - [Raw notebook](https://gitlab.com/lerichi/applied-process-control-and-simulation/-/blob/main/synchro-kalman-observer.ipynb)

   * **Extended Kalman Filter for LVDT Transmitter**: An Extended Kalman Filter is used to track displacement and speed of an LVDT transmitter

     View on:
      - [GitLab Pages](https://lerichi.gitlab.io/applied-process-control-and-simulation/ekf-lvdt-demodulator.html)
      - [nbviewer.org](https://nbviewer.org/urls/gitlab.com/lerichi/applied-process-control-and-simulation/-/raw/main/ekf-lvdt-demodulator.ipynb)
      - [Raw notebook](https://gitlab.com/lerichi/applied-process-control-and-simulation/-/blob/main/ekf-lvdt-demodulator.ipynb)

   * **Extended Kalman Filter for LVDT Transmitter with phase shift**: An Extended Kalman Filter is used to track displacement of an LVDT transmitter having phase shift between the signals

     View on:
      - [GitLab Pages](https://lerichi.gitlab.io/applied-process-control-and-simulation/ekf-lvdt-demodulator-with-phase-shift.html)
      - [nbviewer.org](https://nbviewer.org/urls/gitlab.com/lerichi/applied-process-control-and-simulation/-/raw/main/ekf-lvdt-demodulator-with-phase-shift.ipynb)
      - [Raw
        notebook](https://gitlab.com/lerichi/applied-process-control-and-simulation/-/blob/main/ekf-lvdt-demodulator-with-phase-shift.ipynb)

## TODO
- [x] Simple harmonic oscillator
- [x] Pendulum equations of motion using Lagrangian mechanics
- [x] Kalman Filter for synchro transmitter
- [ ] Adiabatic compression and Kalman observer
- [ ] PI Control with saturation and linearization. Point of load hydraulic pressure conditioning
- [ ] Robust position control
- [ ] Tuned fork comb filter
- [ ] Tripod control with Extended Kalman Filter as observer
- [ ] Controlling hydraulic cylinder

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
