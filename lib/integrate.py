from scipy.integrate import solve_ivp as _rk45
from dataclasses import dataclass
import typing
import numpy as np

@dataclass
class ode45:
    """Single step ODE integration state wrapper
    
    This class is used to integrate a function numerically given a derivative
    function.  It can be used in a with-context
    
    Use case:
        def derivative(t, x, *args):
            F = args[0]
            return [t*F]
            
        with Integrator(derivative, (4, )) as f:
            x1 = f.next(1, 3)
    
    Arguments:
        derivative: a function that return the state derivative.
                    Call signature: derivative(t, x, *args)
        state: initial state
    """

    derivative: typing.Callable[[np.array, np.array, typing.Any], list[float]]
    state: np.array

    __t: float = 0.

    def __enter__(self):
        """Context entry"""
        return self
    
    def __exit__(*_):
        """Context cleanup"""
        pass
    
    def _state(self):
        return self.__x.copy()
    
    def _derivative(self, t, x, *args):
        """Return derivative at a point"""
        return self.__derivative(t, x, *self.__args)
    
    def next(self, t, *args):
        """Return next integration step up to t"""
        self.state = _rk45(self.derivative,
                (self.__t,t), self.state, args=args).y[:,-1]
        self.__t = t
        return self.state
